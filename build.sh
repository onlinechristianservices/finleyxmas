
	dotnet publish -o publish -c Release
	docker build -t ocsregistry.azurecr.io/finleyxmas:v$1 .
	az acr login -n ocsregistry
	docker push ocsregistry.azurecr.io/finleyxmas:v$1

	sed -i '' "s/:v[[:digit:]]*/:v$1/" ./k8s.yaml

	echo "k8s.yaml has been updated for image version $1. 'kubectl apply' at your convenience"
